package com.twuc.webApp.yourTurn;

import com.twuc.webApp.model.AbstractBaseClass;
import com.twuc.webApp.model.Animal;
import com.twuc.webApp.model.DerivedClass;
import com.twuc.webApp.model.Dog;
import com.twuc.webApp.model.InterfaceOne;
import com.twuc.webApp.model.InterfaceOneImpl;
import com.twuc.webApp.model.Prototype;
import com.twuc.webApp.model.PrototypeScopeDependsOnSingleton;
import com.twuc.webApp.model.SimplePrototypeScopeClass;
import com.twuc.webApp.model.SingletonCanBeGet;
import com.twuc.webApp.model.SingletonDependsOnPrototype;
import com.twuc.webApp.model.SingletonDependsOnPrototypeProxy;
import com.twuc.webApp.model.SingletonDependsOnPrototypeProxyBatchCall;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

class ScopeTest {
    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void createApplicationContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_return_same_instance_for_interface() {
        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);
        InterfaceOneImpl interfaceOneImpl = context.getBean(InterfaceOneImpl.class);
        assertSame(interfaceOne, interfaceOneImpl);
    }

    @Test
    void should_return_same_instance_for_extends() {
        Animal animal = context.getBean(Animal.class);
        Dog dog = context.getBean(Dog.class);
        assertSame(animal, dog);
    }

    @Test
    void should_return_same_instance_for_abstract_extends() {
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstractBaseClass, derivedClass);
    }

    @Test
    void should_return_not_same_object_when_bean_scope_is_prototype() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass anotherSimplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(simplePrototypeScopeClass, anotherSimplePrototypeScopeClass);
    }

    /*
    singleton 扫描时创建
    prototype 是getBean()时创建
     */
    @Test
    void should_create_singleton_when_scan_and_create_prototype_when_get_bean() {
        Prototype prototype = context.getBean(Prototype.class);
        context.getBean(Prototype.class);
        List<String> logs = prototype.getLogger().getLogs();
        List<String> singletonList = logs.stream().filter(log -> log.equals("singleton()")).collect(toList());
        List<String> prototypeList = logs.stream().filter(log -> log.equals("prototype()")).collect(toList());
        assertEquals(1, singletonList.size());
        assertEquals(2, prototypeList.size());
    }

    @Test
    void should_get_singleton_object_when_get_bean() {
        SingletonCanBeGet bean = context.getBean(SingletonCanBeGet.class);
        context.getBean(SingletonCanBeGet.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("singletonCanBeGet()")).collect(toList());
        assertEquals(2, collect.size());
    }

    @Test
    void should_create_one_singleton_and_create_two_prototype_when_prototype_dependency_singleton() {
        PrototypeScopeDependsOnSingleton prototypeScopeDependsOnSingleton = context.getBean(PrototypeScopeDependsOnSingleton.class);
        context.getBean(PrototypeScopeDependsOnSingleton.class);
        List<String> logs = prototypeScopeDependsOnSingleton.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("singletonDependent()")).collect(toList());
        List<String> collect1 = logs.stream().filter(log -> log.equals("prototypeScopeDependsOnSingleton()")).collect(toList());
        assertEquals(1, collect.size());
        assertEquals(2, collect1.size());
    }

    @Test
    void should_create_one_singleton_and_one_prototype_when_singleton_dependency_prototype() {
        SingletonDependsOnPrototype singletonDependsOnPrototype = context.getBean(SingletonDependsOnPrototype.class);
        context.getBean(SimplePrototypeScopeClass.class);
        List<String> logs = singletonDependsOnPrototype.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("prototypeDependent()")).collect(toList());
        List<String> collect1 = logs.stream().filter(log -> log.equals("singletonDependsOnPrototype()")).collect(toList());
        assertEquals(1, collect.size());
        assertEquals(1, collect1.size());
    }

    @Test
    void should_create_another_prototype_object_when_invoke_prototype_object_instance_method() {
        SingletonDependsOnPrototypeProxy singletonDependsOnPrototypeProxy = context.getBean(SingletonDependsOnPrototypeProxy.class);
        singletonDependsOnPrototypeProxy.getPrototypeDependentWithProxy().invoke();
        singletonDependsOnPrototypeProxy.getPrototypeDependentWithProxy().invoke();
        List<String> logs = singletonDependsOnPrototypeProxy.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("singletonDependsOnPrototypeProxy()")).collect(toList());
        List<String> collect1 = logs.stream().filter(log -> log.equals("prototypeDependentWithProxy()")).collect(toList());
        assertEquals(1, collect.size());
        assertEquals(2, collect1.size());
    }

    @Test
    void should_create_two_prototype_object_when_invoke_no_independent_implementation_method_twice() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.invokeNoIndependentImplementation();
        List<String> logs = bean.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("prototypeDependentWithProxy()")).collect(toList());
        assertEquals(2, collect.size());
    }

    @Test
    void should_not_create_target_object_when_use_proxy_mode() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("prototypeDependentWithProxy()")).collect(toList());
        assertEquals(0, collect.size());
    }

    @Test
    void should_not_create_target_object_when_invoke_field() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.invokeField();
        List<String> logs = bean.getLogger().getLogs();
        Optional<String> first = logs.stream().filter(log -> log.equals("prototypeDependentWithProxy()")).findFirst();
        assertFalse(first.isPresent());
    }
}
