package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyLogger {

    private List<String> logs = new ArrayList<>();

    void log(String msg) {
        logs.add(msg);
    }

    public List<String> getLogs() {
        return logs;
    }
}
