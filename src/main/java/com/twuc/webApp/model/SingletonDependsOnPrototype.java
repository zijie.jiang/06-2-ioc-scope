package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototype {
    private final MyLogger logger;
    private final PrototypeDependent prototypeDependent;

    public SingletonDependsOnPrototype(MyLogger logger, PrototypeDependent prototypeDependent) {
        this.logger = logger;
        this.prototypeDependent = prototypeDependent;
        logger.log("singletonDependsOnPrototype()");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
