package com.twuc.webApp.model;


import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    private final PrototypeDependentWithProxy prototypeDependentWithProxy;
    private final MyLogger logger;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy, MyLogger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        this.logger = logger;
        logger.log("singletonDependsOnPrototypeProxy()");
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
