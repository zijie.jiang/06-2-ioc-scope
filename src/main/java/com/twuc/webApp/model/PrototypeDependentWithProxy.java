package com.twuc.webApp.model;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@Component
@Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
public class PrototypeDependentWithProxy {

    public int id;

    private final MyLogger logger;

    public PrototypeDependentWithProxy(MyLogger logger) {
        this.logger = logger;
        logger.log("prototypeDependentWithProxy()");
    }

    public void invoke() {

    }
}
