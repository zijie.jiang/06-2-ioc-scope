package com.twuc.webApp.model;


import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SingletonCanBeGet {
    private final MyLogger logger;

    public SingletonCanBeGet(MyLogger logger) {
        this.logger = logger;
        logger.log("singletonCanBeGet()");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
