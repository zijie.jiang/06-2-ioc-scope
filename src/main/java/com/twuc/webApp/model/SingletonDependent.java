package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    private final MyLogger logger;

    public SingletonDependent(MyLogger logger) {
        this.logger = logger;
        logger.log("singletonDependent()");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
