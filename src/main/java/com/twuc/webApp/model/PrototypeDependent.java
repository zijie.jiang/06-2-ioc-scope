package com.twuc.webApp.model;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PrototypeDependent {
    private final MyLogger logger;

    public PrototypeDependent(MyLogger logger) {
        this.logger = logger;
        logger.log("prototypeDependent()");
    }
}
