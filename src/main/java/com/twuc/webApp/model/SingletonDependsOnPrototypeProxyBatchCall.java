package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
    private final MyLogger logger;
    private final PrototypeDependentWithProxy prototypeDependentWithProxy;

    public SingletonDependsOnPrototypeProxyBatchCall(MyLogger logger, PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.logger = logger;
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        logger.log("singletonDependsOnPrototypeProxyBatchCall()");
    }

    public MyLogger getLogger() {
        return logger;
    }

    public void invokeNoIndependentImplementation() {
        this.prototypeDependentWithProxy.toString();
        this.prototypeDependentWithProxy.toString();
    }

    public int invokeField() {
        return this.prototypeDependentWithProxy.id;
    }
}
