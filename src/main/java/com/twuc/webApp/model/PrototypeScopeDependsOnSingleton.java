package com.twuc.webApp.model;


import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {
    private final SingletonDependent singletonDependent;
    private final MyLogger logger;

    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent, MyLogger logger) {
        this.singletonDependent = singletonDependent;
        this.logger = logger;
        logger.log("prototypeScopeDependsOnSingleton()");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
